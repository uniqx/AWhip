<!--
SPDX-FileCopyrightText: 2022 Michael Pöhn <michael.poehn@fsfe.org>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- German translation

### Changed
- used dots inidcator instead for matieral view pager inidcator

### Fixed
- fix occasionally delayed audio playback
- updated deprecated usage note

## [v1.1.0] - 2022-02-05
### Changed
- App support for swing detection on a way broder range of phones (switch from
  gyroscope to acclerometer sensor data)
- support for left handed users (use polar coordinates instead of caretesian
  coordinates for interpreting sensor data)
- upgrated project to newest version of android build tools, androidx, app compat, etc.

[Unreleased]: https://codeberg.org/uniqx/AmSprung/src/branch/master
[v1.1.0]: https://codeberg.org/uniqx/AmSprung/src/tag/v1.1.0
