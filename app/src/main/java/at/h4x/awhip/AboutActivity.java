package at.h4x.awhip;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView librariesBlock = findViewById(R.id.libraries_block);
        TextView licenseBlock = findViewById(R.id.license_block);
        TextView upperBlock = findViewById(R.id.upper_block);

        StringBuilder sb = new StringBuilder();
        sb.append("<b>");
        sb.append(getString(R.string.warning_title));
        sb.append(":</b> ");
        sb.append(getString(R.string.warning));
        sb.append("<br/><br/><b>");
        sb.append(getString(R.string.about_text_title));
        sb.append(":</b> ");
        sb.append(getString(R.string.about_text));
        sb.append("<br/><br/><b>");
        sb.append(getString(R.string.usage_text_title));
        sb.append(":</b> ");
        sb.append(getString(R.string.usage_text));
        sb.append("<br/><br/><b>");
        sb.append(getString(R.string.privacy_notice_title));
        sb.append(":</b> ");
        sb.append(getString(R.string.privacy_notice));
        sb.append("<br/><br/><b>");
        sb.append(getString(R.string.links_website_title));
        sb.append(":</b> <a href=\"");
        sb.append(getString(R.string.links_website));
        sb.append("\">");
        sb.append(getString(R.string.links_website));
        sb.append("</a><br/><b>");
        sb.append(getString(R.string.links_donations_title));
        sb.append(":</b> <a href=\"");
        sb.append(getString(R.string.links_donations));
        sb.append("\">");
        sb.append(getString(R.string.links_donations));
        sb.append("</a>");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            upperBlock.setText(Html.fromHtml(sb.toString(), Html.FROM_HTML_MODE_COMPACT));
            licenseBlock.setText(Html.fromHtml(getString(R.string.license_note), Html.FROM_HTML_MODE_COMPACT));
            librariesBlock.setText(Html.fromHtml(getString(R.string.libraries_text), Html.FROM_HTML_MODE_COMPACT));
        } else {
            upperBlock.setText(Html.fromHtml(sb.toString()));
            licenseBlock.setText(Html.fromHtml(getString(R.string.license_note)));
            librariesBlock.setText(Html.fromHtml(getString(R.string.libraries_text)));
        }

        upperBlock.setMovementMethod(LinkMovementMethod.getInstance());
        licenseBlock.setMovementMethod(LinkMovementMethod.getInstance());
        librariesBlock.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
