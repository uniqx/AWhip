package at.h4x.awhip;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class WhipDetailsDialogFragment extends BottomSheetDialogFragment {

    private WhipDef whipDef;

    public static WhipDetailsDialogFragment newInstance(WhipDef whipDef) {
        final WhipDetailsDialogFragment fragment = new WhipDetailsDialogFragment();
        fragment.setArguments(WhipDef.toBundle(whipDef));
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            whipDef = WhipDef.fromBundle(getArguments());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_whipdetails_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        ((TextView) view.findViewById(R.id.title)).setText(whipDef.getTitleId());
        TextView detailsTextView = (TextView) view.findViewById(R.id.details);
        detailsTextView.setText(whipDef.getDetailsId());
        // enable html links in this text view
        detailsTextView.setMovementMethod(LinkMovementMethod.getInstance());

        view.findViewById(R.id.action_about).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                startActivity(new Intent(getActivity(), AboutActivity.class));
            }
        });

        view.findViewById(R.id.action_help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                startActivity(new Intent(getActivity(), IntroActivity.class));
            }
        });

        view.findViewById(R.id.action_play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).playWhipSound();
            }
        });
    }
}
