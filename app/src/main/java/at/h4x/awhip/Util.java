package at.h4x.awhip;

public class Util {
    public static boolean between(float x, float min, float max) {
        return x >= min && x <= max;
    }
}
