package at.h4x.awhip;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class WhipFragment extends Fragment {

    private WhipDef whipDef;

    public WhipFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance(WhipDef whipDef) {
        WhipFragment fragment = new WhipFragment();
        fragment.setArguments(WhipDef.toBundle(whipDef));
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            whipDef = WhipDef.fromBundle(getArguments());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_whip, container, false);
        ((TextView) view.findViewById(R.id.title)).setText(whipDef.getTitleId());

        if (whipDef.getDrawableId() != 0) {
            ImageView centerPic = view.findViewById(R.id.center_pic);
            centerPic.setImageResource(whipDef.getDrawableId());
            centerPic.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        }


        return view;
    }
}
