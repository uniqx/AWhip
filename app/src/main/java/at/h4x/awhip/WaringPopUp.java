package at.h4x.awhip;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;

import androidx.appcompat.app.AlertDialog;

public class WaringPopUp {


    private static final String PREF_DIALOG_STATE = "dialog_state";
    private static final String DEFAULT_DIALOG_STATE = DialogState.SHOW_NOT_DISSMISSABLE.toString();

    public static void go(final Context context) {

        final SharedPreferences prefs = context.getSharedPreferences("warning_popup.prefs", Context.MODE_PRIVATE);
        DialogState dialogState = DialogState.fromString(prefs.getString(PREF_DIALOG_STATE, DEFAULT_DIALOG_STATE));

        if (!DialogState.HIDDEN.equals(dialogState)) {

            View layout = LayoutInflater.from(context).inflate(R.layout.dialog_warning, null);
            final CheckBox checkBox = layout.findViewById(R.id.dontShowAgain);

            if (DialogState.SHOW_NOT_DISSMISSABLE.equals(dialogState)) {
                checkBox.setVisibility(View.GONE);

                prefs.edit().putString(PREF_DIALOG_STATE, DialogState.SHOW_DISMISSABLE.toString()).commit();
            } else {
                checkBox.setVisibility(View.VISIBLE);
            }

            final DialogInterface.OnClickListener understoodListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (checkBox.isChecked()) {
                        prefs.edit().putString(PREF_DIALOG_STATE, DialogState.HIDDEN.toString()).commit();
                    }
                }
            };

            new AlertDialog.Builder(context)
                    .setTitle(R.string.warning_title)
                    .setMessage(R.string.warning)
                    .setView(layout)
                    .setCancelable(false)
                    .setPositiveButton(R.string.understood, understoodListener)
                    .create().show();
        }
    }

    enum DialogState {
        SHOW_NOT_DISSMISSABLE("not-dismissable"),
        SHOW_DISMISSABLE("dismissable"),
        HIDDEN("hidden");

        private final String val;

        DialogState(String val) {
            this.val = val;
        }

        @Override
        public String toString() {
            return val;
        }

        public static DialogState fromString(String val) {
            for(DialogState ds : DialogState.values()) {
                if (ds.val.equals(val)) {
                    return ds;
                }
            }
            // default to SHOW_NOT_DISSMISSABLE
            return SHOW_NOT_DISSMISSABLE;
        }
    }
}
