package at.h4x.awhip;

import android.os.Bundle;
import androidx.annotation.DrawableRes;
import androidx.annotation.RawRes;
import androidx.annotation.StringRes;

public class WhipDef {

    public static final WhipDef[] list = new WhipDef[]{
            // TODO: re-enable and finish deactivated whips
            //new WhipDef(R.string.longeing, R.string.longeing_details, R.drawable.longeing_whip_sketch, 0),
            new WhipDef(R.string.bull_whip, R.string.bull_whip_details, R.drawable.bull_whip_sketch, R.raw.peterbullmusic_mixed_whip_crack_1),
            new WhipDef(R.string.riding_crop, R.string.riding_crop_details, R.drawable.riding_crop_sketch, R.raw.jacksonml_whip_1),
            //new WhipDef(R.string.catonine, R.string.catonine_details, R.drawable.catonine_sketch, 0),
            //new WhipDef(R.string.spencer, R.string.spencer_details, 0, R.raw.ekokubza123_punch)
    };

    private static final String ARG_TITLE_ID = "param_title_id";
    public static final String ARG_DETAILS_ID = "param_details_id";
    private static final String ARG_DRAWABLE_ID = "param_drawable_id";
    private static final String ARG_AUDIO_FILE_ID = "param_audio_file_id";

    private final int titleId;
    private final int detailsId;
    private final int drawableId;
    private final int audioFileId;

    public WhipDef(@StringRes int titleId, @StringRes int detailsId, @DrawableRes int drawableId, @RawRes int audioFileId) {
        this.titleId = titleId;
        this.detailsId = detailsId;
        this.drawableId = drawableId;
        this.audioFileId = audioFileId;
    }

    public int getTitleId() {
        return titleId;
    }

    public int getDetailsId() {
        return detailsId;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public int getAudioFileId() {
        return audioFileId;
    }

    public static WhipDef fromBundle(Bundle bundle) {
        return new WhipDef(
                bundle.getInt(ARG_TITLE_ID),
                bundle.getInt(ARG_DETAILS_ID),
                bundle.getInt(ARG_DRAWABLE_ID),
                bundle.getInt(ARG_AUDIO_FILE_ID));
    }

    public static Bundle toBundle(WhipDef whipDef) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_TITLE_ID, whipDef.getTitleId());
        bundle.putInt(ARG_DETAILS_ID, whipDef.getDetailsId());
        bundle.putInt(ARG_DRAWABLE_ID, whipDef.getDrawableId());
        bundle.putInt(ARG_AUDIO_FILE_ID, whipDef.getAudioFileId());
        return bundle;
    }
}
