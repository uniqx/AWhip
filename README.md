<!--
SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# A Whip

Turn your phone into a whip

<img alt="logo" src="https://codeberg.org/uniqx/AWhip/raw/branch/main/app/src/main/ic_awhip_launcher-web.png" height="60" />

<a href="https://f-droid.org/packages/at.h4x.awhip">
  <img alt="Get it on F-Droid" src="https://codeberg.org/uniqx/AWhip/media/branch/main/dev-assets/fdroid-badge.png" height="60" />
</a>

<a href="https://liberapay.com/uniqx/donate">
  <img alt="Donate on Liberapay" src="https://codeberg.org/uniqx/AWhip/media/branch/main/dev-assets/liberapay-donate.png" height="60" />
</a>

## Screenshots

<a href="https://codeberg.org/uniqx/AWhip/media/branch/main/metadata/en-US/images/phoneScreenshots/screenshot1.png">
  <img alt="Screenshot 1" src="https://codeberg.org/uniqx/AWhip/media/branch/main/metadata/en-US/images/phoneScreenshots/screenshot1.png" width="150" />
</a>

<a href="https://codeberg.org/uniqx/AWhip/media/branch/main/metadata/en-US/images/phoneScreenshots/screenshot2.png">
  <img alt="Screenshot 2" src="https://codeberg.org/uniqx/AWhip/media/branch/main/metadata/en-US/images/phoneScreenshots/screenshot2.png" width="150" />
</a>

## License

GPL-3.0-or-later
